# FRC CHIPS `git` and Gitlab Training

Welcome! This is Chips' Gitlab training project :)

First things first, let's cover the basics:

## What is `git`?

**Git is a version control system** that makes sure everyone working on a software project has all the same files and versions of files. 
Tracking projects in git is a lot like writing a group report on google docs, except you have to explicitly "commit" your changes for everyone else to see. 
Everyone can work on different parts of the code base at once, and git can track all the different changes. 
Plus, git won't let teammates accidentally overwrite each other's work if they work on the same part of the project at the same time! 

## Why use `git`?

The biggest reasons are: 

1. **All changes must be explicitly committed.** This encourages developers to review their own changes before committing, and prevents typos and unfinished work from sneaking in.
2. **Parallel Development.** Development in git takes place on *branches*, such that different versions of the codebase can exist at once. For example, some people can work on a "drive base" branch while others work on a "shooter" branch. That way if the shooter code isn't done yet, the drive base team can test and integrate without having to wait for them. Or, a team can selectively merge in *only some* committed changes if the rest aren't ready.
3. **Stable Development.** Because branches allow us to work on different branches, we can maintain a stable branch that only has test-verified code that is guaranteed to work, even if all our development branches are broken.

## What is Gitlab?

Gitlab is a website that hosts "remote" copies of git repositories. Because Gitlab exists on the world wide web, everyone can push code to a place where everyone else can pull changes down. This is much more secure and practical than connecting computer to computer or passing around a flash drive.

Gitlab also comes with handy-dandy administrative tools, which can:

1. Prevent unauthorized people from pushing changes to our project
2. Help us keep track of issues and organize work tasks
3. Provide an interaction environment for testing, approving, and merging code when it's ready.

## How do I get started with git?

1. *Create a gitlab account, and ask a software mentor to add you to the team gitlab group.*

	The mentor will add you as a **reporter** to every repository except this one (`gitlab_training`), which you must be made a **developer** to complete training tasks. Being a **reporter** means you will be able to download our repos and make local changes, but you cannot push those changes back to gitlab. Patience you must have, my young padawan. You will become a certified **developer** when you complete this training. I promise.

2. *If not already done, install git on the computer you will develop on. Use the default settings unless you know what you are doing.*

	- [Windows: https://git-scm.com/download/win](https://git-scm.com/download/win)

	- RHEL/Fedora: `sudo dnf install git-all`
				 OR `sudo yum install git` (whichever works first)

	- Debian: `sudo apt install git-all`

	- macOS: Install Xcode, which ships with a git package OR install the homebrew RPM at your own peril and `brew install git`

3. *Install any git graphical user interface (GUI) front-end you would like. These aren't necessary if you're comfy working in a command line interface (CLI) terminal, like `bash`*

	- Mac, Linux, and Windows: [VScode: https://code.visualstudio.com/download](https://code.visualstudio.com/download)

	- Windows: [TortoiseGit: https://tortoisegit.org/download/](https://tortoisegit.org/download/)

4. *[Watch this video. It is not comprehensive, but it is a great primer.](https://www.youtube.com/watch?v=USjZcfj8yxE)*

5. *Read, understand, and remember the git rules and git commands below.*

6. *Read `GIT_TO_WORK.md` and complete the certification tasks. Once a mentor checks your work, you will be granted **developer** status. Get to work!*

## Git & Gitlab rules

1. *With great power comes great responsibility.* Know the **correct** answers to the questions: Can I? Should I? Will I?

2. *Be aware.* Be aware of others working on the same area as you. Do not make changes that conflict with theirs. Be aware of others working on NOT the same area as you. Do not make changes that will pull the carpet out from under them without discussing it with them first. Be aware of what is NOT being done. If you submitted a merge request for finished code to be tested on the robot two weeks ago and no one has touched it, tell the approver! Tell a mentor! Tell someone!

3. *Be courteous.* Be courteous of others time and efforts. If you see potential incompatibilities, shortcomings, bugs, improvements, TELL THEM. Create a gitlab issue and mark it with the appropriate labels, and tag anyone involved in descriptions and comments to start a dialogue. Proposals, critiques, and concerns are productive. Arguments, opinions, and complaints are not.

4. *Be clear.* Be clear in your code. Inline comments, summary headers, and meaningful variable/object names are REQUIRED in every situation. `x` is not a good variable name. `horizontal_distance` is. Be clear in your commit messages, merge requests, branch names, and gitlab contents. Anyone should be able to look at your work and understand what it's supposed to do, even if it's not immediately obvious how it works under the hood. If you commit something without a meaningful commit message, I will use my h4x0r skills to track you down and make you write a 5 page essay on why a hot dog technically counts as a sandwich. And I will find you pretty easily. Your digital signature is attached to everything you `git`.

5. *Keep it clean.* There's a lot of people working on this project. Don't leave a whole bunch of half finished work getting stale. Focus on a single task at a time. It will be a nightmare to try and figure out inter-dependencies between a dozen simultaneous changes. Get it done and git it done. DO NOT COMMIT files that should not be kept in the repo for eternity. This includes build artifacts (i.e. binary files and executables that only work with your computer), external libaries/dependencies, and temporary files.

## Git Commands that you MUST know to succeed as a git guru

* `git clone <URL>`: Create a local copy of a remote repository. This is how you create a local copy of a git repository in order to communicate with gitlab. In general, you should only ever need to do this once for a given repository.

* `git checkout <branch_name>`: Change to a different `branch` (i.e. the 'version' of the project you have active right now).

* `git checkout <branch_name> -- <file_names>`: Stay on the current branch, but checkout the specified files from the specified branch.

* `git add <-u> <file_or_folder_names>`: This is how you tell git "This one of the things I want to keep." Stage the specified files/folders for a `git commit`. `-u` optionally includes only '`u`pdated` files. This will prevent you from adding untracked files, so use it appropriately.

* `git reset <file_or_folder>`: un-`add` the files or folders specified. If no files provided, resets everything.

* `git commit -m "<commit message here>"`: This is analogous to File->Save. This permanently creates a record of the repo in its current state, which can be `git push`ed to remote repos (i.e. gitlab)

* `git push <origin> <branch_name>`: Upload the commits on my local `branch_name` to the remote repository called `origin` (default). Basically: share commits to others.

* `git pull <origin> <branch_name>`: Download the commits on the remote `origin`'s version of `branch_name` into my local version of `branch_name`, if crossing branches (i.e. I have branch `bbb` checked out and I call `git pull origin aaa`), an automatic merge will be attempted. If unfetched changes between the local and remote (i.e. you and someone else both made commits on this branch since the last pull/push), an automatic merge will be attempted. If the merge fails, get an adult unless you *REALLY* know what you're doing.

* `git branch <new_branch_name>`: create a new branch with the specified name.

* `git branch <-a>`: list the available branches. `-a` optionally includes '`a`ll' branches, including those of remote repositories (i.e. branches on gitlab)

* `git status`: Get the current state of the repository. This will show:
	*  **current branch** that you have `git checkout`ed. If you've committed but not pushed, it will tell you that you're ahead of the `remote` branch (i.e. gitlab's version usually called `origin`). If you've `git fetch`ed but not `git pull`ed, it will tell you're behind the remote. NOTE: just because `git status` says you're up to date with the `remote` does not mean you have the same version as what's on gitlab. It just means you were up to date last time you checked. Do a `git fetch origin` to check again.
	*  **Untracked files** or folders not previously committed to the repository
	*  **Modified files**: `Changes not staged for commit`. Files that have changed since the last commit, and are not staged for an upcoming commit.
	*  **Staged files**: `Changes to be committed`. Files that have changed since the last commit, and are `git add`ed to the stage, meaning they *will be committed* at the next commit.